using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBrain : MonoBehaviour
{
    // Start is called before the first frame update

    public float SpeedEnemy = 1.5f;
    public LayerMask LayerGround;
    private bool _faceRight;
    private Rigidbody2D _rigidBody;
    private Weapon _weapon;
    public Vector2 direction;
    private bool _isAttacking;
    private Animator _animator;
    public Transform PointCollider;
    public float RadiusCollider = 0.5f;
    public float AimingTime = 0.5f;
    public float ShootingTime = 1.5f;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _weapon = GetComponentInChildren<Weapon>();
        _animator = GetComponent<Animator>();
    }

    void Start()
    {

        if( transform.localScale.x<0f)
        {
            _faceRight = false;
        }
        else if(transform.localScale.x>0f)
        {
            _faceRight = true;
        }

    }

    // Update is called once per frame
    void Update()
    {
        direction = Vector2.right;

        
        if(_faceRight==false )
        {
            direction = Vector2.left;
        }

        if (_isAttacking == false && Physics2D.Raycast(PointCollider.position, direction, RadiusCollider, LayerGround))
        {    
            Flip();
        }
    }

    private void FixedUpdate()
    {
        float horizontalVelocity = SpeedEnemy;

        if(_faceRight == false)
        {
            horizontalVelocity = horizontalVelocity * -1f;
        }

        _rigidBody.velocity = new Vector2(horizontalVelocity * SpeedEnemy, _rigidBody.velocity.y);
    }

    private void LateUpdate()
    {
        _animator.SetBool("Idle", _rigidBody.velocity == Vector2.zero);

    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (_isAttacking==false && collision.CompareTag("Player"))
        {
            StartCoroutine("EnemyShooting");
        }
    }

    private IEnumerator EnemyShooting()
    {
        

        float SpeedBackup = SpeedEnemy;
        SpeedEnemy = 0f;
        _isAttacking = true;

        yield return new WaitForSeconds(AimingTime);

        _animator.SetTrigger("Shoot");

        yield return new WaitForSeconds(ShootingTime);

        SpeedEnemy = SpeedBackup;

        _isAttacking = false;
    }

    private void Flip()
    {
        _faceRight = !_faceRight;
        float localScales = transform.localScale.x;
        localScales = localScales * -1;
        transform.localScale = new Vector3(localScales, transform.localScale.y, transform.localScale.z);
    }

    
    private void CanShoot()
    {
        if(_weapon!= null)
        {
            _weapon.Shoot();
        }
    }
}
