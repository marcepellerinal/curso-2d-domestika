using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    // Start is called before the first frame update
    public float speed = 2.5f;
    private Animator _animator;
    private Rigidbody2D _rigidBody;
    private bool _faceright = true;
    private Vector2 _movement;
    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _rigidBody = GetComponent<Rigidbody2D>();
    }

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        float horizontalInput = Input.GetAxisRaw("Horizontal");
       
            _movement = new Vector2(horizontalInput, 0f);

            if (horizontalInput<0f && _faceright == true)
            {
                Flip();
            }
            else if(horizontalInput>0f && _faceright == false)
            {
                Flip();
            }
    }

    private void FixedUpdate()
    {
        float horizontalVelocity = _movement.normalized.x * speed;
        _rigidBody.velocity = new Vector2(horizontalVelocity, _rigidBody.velocity.y);
    }

    private void LateUpdate()
    {
        _animator.SetBool("Idle", _movement == Vector2.zero);
    }

    void Flip()
    {
        _faceright = !_faceright;
        float _localscale = transform.localScale.x;
        _localscale = _localscale * -1f;
        transform.localScale = new Vector3(_localscale, transform.localScale.y, transform.localScale.z);
    }
}
