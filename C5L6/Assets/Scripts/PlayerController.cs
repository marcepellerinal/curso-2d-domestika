using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private Rigidbody2D _rigidBody;
    private Animator _animator;
    private Vector2 _movement;
    public float speed = 2.5f;
    private bool _faceRight = true;

    public float JumForce= 2.5f;
    public Transform CheckPosition;
    public LayerMask CheckLayer;
    public float CheckRadius;
    private bool _isGrounded;
    private bool _isAttacking;
    private float _longIdleTimer;
    public float longIdle = 5f;

    private void Awake()
    {
        _rigidBody = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(_isAttacking == false)
        {
            float horizontalInput = Input.GetAxisRaw("Horizontal");
            _movement = new Vector2(horizontalInput, 0f);

            if (horizontalInput < 0f && _faceRight == true)
            {
                Flip();
            }
            else if (horizontalInput > 0f && _faceRight == false)
            {
                Flip();
            }

        }

        _isGrounded = Physics2D.OverlapCircle(CheckPosition.position, CheckRadius, CheckLayer);

        if(Input.GetButtonDown("Jump") && _isGrounded == true && _isAttacking == false)
        {
            _rigidBody.AddForce(Vector2.up * JumForce, ForceMode2D.Impulse);
        }

        if(Input.GetButtonDown("Fire1") && _isGrounded == true && _isAttacking == false)
        {
            _animator.SetTrigger("Attack");
        }
    }

    private void FixedUpdate()
    {
        if(_isAttacking== false)
        {
            float horizontalVelocity = _movement.normalized.x * speed;
            _rigidBody.velocity = new Vector2(horizontalVelocity, _rigidBody.velocity.y);
        }
      
    }

    private void LateUpdate()
    {
        _animator.SetBool("Idle", _movement == Vector2.zero);
        _animator.SetBool("IsGrounded", _isGrounded);
        _animator.SetFloat("VerticalVelocity", _rigidBody.velocity.y);

        if(_animator.GetCurrentAnimatorStateInfo(0).IsTag("Attacking"))
        {
            _isAttacking = true;
        }
        else
        {
            _isAttacking = false;
        }
        if(_animator.GetCurrentAnimatorStateInfo(0).IsTag("Idle"))
        {
            _longIdleTimer += Time.deltaTime;

               if(_longIdleTimer >= longIdle)
                {
                _animator.SetTrigger("LongIdle");
                }
        }
        else
        {
            _longIdleTimer = 0f;
        }
    }

    private void Flip()
    {
        _faceRight = !_faceRight;
        float localScale = transform.localScale.x;
        localScale = localScale * -1f;
        transform.localScale = new Vector3(localScale, transform.localScale.y, transform.localScale.z);
    }

}
