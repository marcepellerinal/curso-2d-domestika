using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControlito : MonoBehaviour
{
    private Animator _animator;
    private Rigidbody2D _rigidBody;
    private Vector2 _movement;
    public float speed= 2.5f;
    private bool _faceright = true;

    public float JumpForce = 4f;
    public Transform checkPosition;
    public LayerMask CheckLayer;
    public float checkRadius;
    private bool _isGrounded;

    private bool _isAttacking;

    public float LongIdleTimer = 4f;
    private float _LongIdle;

    private void Awake()
    {
        _animator = GetComponent<Animator>();
        _rigidBody = GetComponent<Rigidbody2D>();
    }


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
      if(_isAttacking== false)
        {
            float horizontalInput = Input.GetAxisRaw("Horizontal");
            _movement = new Vector2(horizontalInput, 0f);

            if (horizontalInput < 0f && _faceright == true)
            {
                Flip();
            }
            else if (horizontalInput > 0f && _faceright == false)
            {
                Flip();
            }
        }
        
        

        _isGrounded = Physics2D.OverlapCircle(checkPosition.position, checkRadius, CheckLayer);

        if (Input.GetButtonDown("Jump") && _isGrounded==true && _isAttacking ==false)
        {
            _rigidBody.AddForce(Vector2.up * JumpForce, ForceMode2D.Impulse);
        }
        
        if(Input.GetButtonDown("Fire1") && _isGrounded==true && _isAttacking==false)
        {
            _animator.SetTrigger("Attack");
        }

    }

    private void FixedUpdate()
    {
        if (_isAttacking==false)
        {
            float horizontalVelocity = _movement.normalized.x * speed;
            _rigidBody.velocity = new Vector2(horizontalVelocity, _rigidBody.velocity.y);
        }
        
    }
    private void LateUpdate()
    {
        _animator.SetBool("Idle", _movement == Vector2.zero);
        _animator.SetBool("IsGrounded", _isGrounded);
        _animator.SetFloat("VerticalVelocity", _rigidBody.velocity.y);

        if(_animator.GetCurrentAnimatorStateInfo(0).IsTag("Attacking"))
        {
            _isAttacking = true;
        }
        else
        {
            _isAttacking = false;
        }
        if(_animator.GetCurrentAnimatorStateInfo(0).IsTag("Idle"))
        {
            _LongIdle += Time.deltaTime;
            if(_LongIdle>= LongIdleTimer)
            {
                _animator.SetTrigger("LongIdle");
            }
        }
        else
        {
            _LongIdle = 0f;
        }

    }

    void Flip()
    {
        _faceright = !_faceright;
        float localScales = transform.localScale.x;
        localScales = localScales * -1;
        transform.localScale = new Vector3(localScales, transform.localScale.y, transform.localScale.z);
    }

}
