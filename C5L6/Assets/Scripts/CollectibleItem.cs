using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectibleItem : MonoBehaviour
{

    [SerializeField] GameObject LightningParticles;
    [SerializeField] GameObject BurstParticles;

    private SpriteRenderer _renderer;

    private void Awake()
    {
        _renderer = GetComponent<SpriteRenderer>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player"))
        {
            _renderer.enabled = false;
            LightningParticles.SetActive(false);
            BurstParticles.SetActive(true);
            Destroy(this.gameObject, 2f);


        }
    }

    

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
